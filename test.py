""" Test script to check the connections to the OpenVPN server. """

import socket

import psutil


ADDRESS_FAMILY = {
    socket.AF_INET: 'AF_INET',
    socket.AF_INET6: 'AF_INET6',
    socket.AF_UNIX: 'AF_UNIX',
    socket.AF_LINK: 'AF_LINK',
}
SOCKET_TYPE = {
    socket.SOCK_STREAM: 'SOCK_STREAM',
    socket.SOCK_DGRAM: 'SOCK_DGRAM',
    socket.SOCK_RAW: 'SOCK_RAW',
    socket.SOCK_RDM: 'SOCK_RDM',
    socket.SOCK_SEQPACKET: 'SOCK_SEQPACKET',
}


if __name__ == '__main__':
    connections = [
        conn for conn in psutil.net_connections(kind='inet')
        if (len(conn.raddr) > 1 and conn.raddr[1] == 1194) or (
            hasattr(conn.raddr, 'port') and conn.raddr.port == 1194
        ) or (
            hasattr(conn.laddr, 'ip') and conn.laddr.ip == '10.8.0.12'
        )
    ]

    for conn in connections:
        # print(
        #     f'raddr type={type(conn.raddr)} raddr={
        #         conn.raddr} laddr={conn.laddr}'
        # )
        print(
            f'pid={conn.pid:04d} laddr={conn.laddr} raddr={
                conn.raddr} status={conn.status} family={
                ADDRESS_FAMILY.get(conn.family, conn.family)} type={
                SOCKET_TYPE.get(conn.type, conn.type)}',
        )
