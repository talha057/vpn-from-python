import socket


def get_ip_addresses():
    print(socket.gethostname())

    for interface in socket.if_nameindex():
        print(interface)
        # continue
        try:
            addresses = socket.getaddrinfo(
                socket.gethostname(),
                None, socket.AF_INET,
                socket.SOCK_DGRAM,
                socket.IPPROTO_IP,
                socket.AI_CANONNAME
            )
            print(addresses)
        except socket.gaierror:
            print(f"Error:")


def main():
    get_ip_addresses()


if __name__ == "__main__":
    main()
