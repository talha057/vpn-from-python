""" Stop the OpenVPN3 CLI process. """

import psutil


def kill_process(name: str):
    """ Kill a process by name. """

    for process in psutil.process_iter(["pid", "name"]):
        try:
            if process.name() != name:
                continue

            process.kill()
            print(f"Killed process {name}.")
            return
        except psutil.ZombieProcess:
            print(f"Process {name} is a zombie process.")
        except psutil.NoSuchProcess:
            print(f"Process {name} does not exist.")
        except psutil.AccessDenied:
            print(f"Access denied to process {name}.")

    print(f"Process {name} not found.")


def main():
    """ Kill the OpenVPN3 CLI process. """

    kill_process("ovpncli")


if __name__ == "__main__":
    main()
