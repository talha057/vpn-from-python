""" Check if VPN is connected and get connection time. """

import datetime
import time

import psutil


def is_connected():
    """ Check if VPN is connected. """

    net_interfaces = psutil.net_if_addrs()
    for interface in filter(lambda x: x.startswith("utun"), net_interfaces.keys()):
        # Check for VPN interface in macOS
        if interface in ["utun0", "utun1", "utun2"]:
            continue

        current_interface = net_interfaces[interface][0]
        if not current_interface.address.startswith("10."):
            return False
        if not current_interface.netmask:
            return False
        if not current_interface.netmask.startswith("255."):
            return False
        if not current_interface.ptp:
            return False
        return True
    return False


def get_connection_time():
    """ Get the connection time of the VPN. """
    current_time = time.time()

    for interface, stats in filter(
        lambda x: x[0].startswith(
            "utun"), psutil.net_io_counters(pernic=True).items()
    ):
        if interface in ["utun0", "utun1", "utun2"]:
            continue

        print(f"Interface: {interface}")
        print(f"Stats: {stats}")
        print(f"Current time: {current_time}")
        print(f"Connection time: {stats[2]}")
        print(f"Time delta: {current_time - stats[2]}")
        # return current_time - stats[2]
    return 0


def main():
    """ Check if VPN is connected and get connection time. """

    if is_connected():
        print("VPN is connected.")
        connection_time = get_connection_time()
        time_delta = datetime.timedelta(seconds=connection_time)
        print(f"Connection time: {time_delta}")
    else:
        print("VPN is not connected.")


if __name__ == "__main__":
    main()
