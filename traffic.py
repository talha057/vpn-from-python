""" Get the total number of bytes sent and received by the system. """

import psutil


def get_traffic():
    """ Get the total number of bytes sent and received by the system. """

    net_io = psutil.net_io_counters()
    return net_io.bytes_sent, net_io.bytes_recv


# def get_process_traffic():
#     net_io = psutil.net_io_counters(pernic=True)
#     return net_io


def get_process_traffic(name: str):
    """ Get the total number of bytes sent and received by a process. """

    for process in psutil.process_iter(["pid", "name"]):
        try:
            if process.name() != name:
                continue

            print(process.is_running())
            print(process.status())
            print(process.create_time())
            print(f"{process.name()}, {process.pid}")
            print(f"{process.parent()}, {process.children()}")
            print(f"{process.connections()}, {process.open_files()}")
            print(f"{process.memory_info()},{process.cpu_times()}")
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return 0, 0


def main():
    """ Get the total number of bytes sent and received by the system. """

    bytes_sent, bytes_recv = get_traffic()
    print(f"Bytes sent: {bytes_sent}")
    print(f"Bytes received: {bytes_recv}")

    process_traffic = get_process_traffic("ovpncli")
    print(f"Process bytes sent: {process_traffic[0]}")
    print(f"Process bytes received: {process_traffic[1]}")


if __name__ == "__main__":
    main()
