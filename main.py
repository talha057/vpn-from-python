"""This is a simple script that gets the VPN status and prints it to the console."""

import sys

import psutil


def detect_os():
    """Detect the OS."""

    if sys.platform.startswith('win32'):
        return 'Windows'
    if sys.platform.startswith('linux'):
        return 'Linux'
    if sys.platform.startswith('darwin'):
        return 'macOS'
    if sys.platform.startswith('cygwin'):
        return 'Cygwin'
    return 'Unknown OS'


def detect_vpn_windows():
    """Detect the VPN status on Windows."""
    for connection in psutil.net_connections(kind='inet'):
        if connection.status == 'ESTABLISHED' and 'vpn' in connection.laddr.ip:
            return 'VPN is connected'
    return 'VPN is not connected'


def detect_vpn_linux():
    """Detect the VPN status on Linux."""
    for connection in psutil.net_connections(kind='inet'):
        if connection.status == 'ESTABLISHED' and connection.laddr.port == 1194:
            return 'VPN is connected'
    return 'VPN is not connected'


def check_vpn_status():
    """Check the VPN status."""
    current_os = detect_os()
    print(f'Current OS: {current_os}')
    if current_os == 'Windows':
        return detect_vpn_windows()
    if current_os in ['Linux', 'macOS']:
        return detect_vpn_linux()
    return 'Unknown OS'


if __name__ == '__main__':
    print(check_vpn_status())
