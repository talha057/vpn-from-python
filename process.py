""" Get process up time """

from datetime import datetime

import psutil


def get_start_time(name: str):
    """ Get the start time of a process. """

    for process in psutil.process_iter(["pid", "name"]):
        try:
            if process.name() != name:
                continue

            return process.create_time()
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            print(f"Process {name} not found.")
            return -1

    print(f"Process {name} not found.")
    return -1


def main():
    """ Get the start time of the OpenVPN3 CLI process. """

    start_time = get_start_time("ovpncli")
    if start_time == -1:
        return
    print(f"Start time: {datetime.fromtimestamp(start_time)}")


if __name__ == "__main__":
    main()
