""" Start the OpenVPN3 CLI with a configuration file. """

import os
import subprocess

import daemon   # type: ignore
import psutil


CURRENT_DIRRECTORY = os.getcwd()
PARENT_DIRECTORY = os.path.dirname(CURRENT_DIRRECTORY)

CONFIG_DIRECTORY = os.path.join(PARENT_DIRECTORY, "build-openvpn3")
CLI_PATH = os.path.join(CONFIG_DIRECTORY, "test", "ovpncli", "ovpncli")
CONFIG_PATH = os.path.join(CURRENT_DIRRECTORY, "config.ovpn")


def start():
    """ Start the OpenVPN3 CLI with a configuration file. """

    try:
        process = subprocess.Popen(
            [CLI_PATH, CONFIG_PATH, "-t", "shark-vpn"],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            stdin=subprocess.DEVNULL,
            start_new_session=True,
            text=True,
        )
        if process.returncode != 0:
            print(f"CLI exited with code {process.returncode}")

        print(f"CLI PID: {process.pid}")

        stdout, stderr = process.stdout, process.stderr
        if stdout:
            print(f"CLI stdout: {stdout}")
        if stderr:
            print(f"CLI stderr: {stderr}")
    except subprocess.CalledProcessError as error:
        print(f"CLI exited with code {error.returncode}")
    except subprocess.TimeoutExpired:
        print("CLI timed out.")
    except subprocess.SubprocessError as error:
        print(f"CLI error: {error}")
    finally:
        print("CLI finished.")


def main():
    """ Run the OpenVPN3 CLI with a configuration file. """

    # Check if the CLI is running already
    for process in psutil.process_iter():
        if process.name() == "ovpncli":
            print("CLI is already running.")
            return

    # Check which utun interface is running
    # the VPN will use the next available utun interface
    # Also skip the first 3 utun interfaces
    net_interfaces = sorted([interface for interface in psutil.net_if_addrs(
    ).keys() if interface.startswith("utun")])
    print(f"Network interfaces: {net_interfaces}")

    next_interface = f"utun{int(net_interfaces[-1][-1]) + 1}"
    print(f"Starting VPN on interface: {next_interface}")

    print(f"Current directory: {CURRENT_DIRRECTORY}")
    print(f"Parent directory: {PARENT_DIRECTORY}")
    print(f"Config directory: {CONFIG_DIRECTORY}")
    print(f"CLI path: {CLI_PATH}")

    # Check if the CLI exists
    if not os.path.exists(CLI_PATH):
        print("CLI does not exist.")
        return

    # Run the CLI
    print("Running CLI...")

    with daemon.DaemonContext(detach_process=True):
        start()

    print("Hello")
    # Check which tunnel is running
    try:
        netstat = subprocess.run(["netstat", "-nr"], check=True)
        print(netstat.stdout)
    except subprocess.CalledProcessError as error:
        print(f"CLI exited with code {error.returncode}")
        return
    except subprocess.TimeoutExpired:
        print("CLI timed out.")
        return


if __name__ == "__main__":
    main()
